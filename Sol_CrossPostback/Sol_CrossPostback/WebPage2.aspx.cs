﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_CrossPostback
{
    public partial class WebPage2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Page.PreviousPage!=null)
            {
                //bad practise
                //TextBox txtBox=(TextBox)PreviousPage.FindControl("txtName");//downcasting done here
                //lblName.Text = txtBox.Text;

                //good practise
                //string strName = ((TextBox)PreviousPage.FindControl("txtName")).Text;
                //lblName.Text = strName;

                //best practise
                lblName.Text= ((TextBox)PreviousPage.FindControl("txtName")).Text;
                lblName1.Text= ((TextBox)PreviousPage.FindControl("txtName1")).Text;
                lblName2.Text= ((TextBox)PreviousPage.FindControl("txtName2")).Text;
                lblName3.Text= ((TextBox)PreviousPage.FindControl("txtName3")).Text;
                lblName4.Text= ((TextBox)PreviousPage.FindControl("txtName4")).Text;
            }
        }
    }
}